<?php

namespace Drupal\Tests\nsw_feedback\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Defines a class for testing sentiment feedback block.
 *
 * @group nsw_feedback
 */
class SentimentBlockTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['nsw_feedback', 'block'];

  /**
   * Tests the sentiment block.
   */
  public function testBlock(): void {
    $label = $this->randomMachineName();
    $description = $this->randomMachineName();
    $source_application = $this->randomMachineName();
    $this->drupalPlaceBlock('nsw_feedback_sentiment', [
      'widget_label' => $label,
      'description' => $description,
      'source_application' => $source_application,
      'environment' => 'uat',
    ]);
    $this->drupalGet('<front>');
    $assert = $this->assertSession();
    $assert->responseContains('quickfeed.min.js');
    $assert->responseContains($label);
    $assert->elementTextContains('css', '.thumbs__description', $description);
    $assert->responseContains($source_application);
  }

}
