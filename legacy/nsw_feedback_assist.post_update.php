<?php

/**
 * @file
 * Contains post update hooks.
 */

use Drupal\block\BlockInterface;
use Drupal\Core\Config\Entity\ConfigEntityUpdater;

/**
 * Enable the correctly named module.
 */
function nsw_feedback_assist_post_update_enable_correct_module() {
  \Drupal::service('module_installer')->install(['nsw_feedback']);
  \Drupal::service('plugin.manager.block')->clearCachedDefinitions();
}

/**
 * Migrate blocks from one module to the next.
 */
function nsw_feedback_assist_post_update_migrate_blocks(&$sandbox) {
  /** @var \Drupal\Core\Config\Entity\ConfigEntityUpdater $updater */
  $updater = \Drupal::classResolver()->getInstanceFromDefinition(ConfigEntityUpdater::class);
  $updater->update($sandbox, 'block', function (BlockInterface $block) {
    $block->calculateDependencies();
    $dependencies = $block->getDependencies();
    if (array_intersect(['nsw_feedback_assist', 'nsw_feedback'], $dependencies['module'] ?? [])) {
      $block->calculateDependencies();
      return TRUE;
    }
    return FALSE;
  });
}

/**
 * Uninstall the old module.
 */
function nsw_feedback_assist_post_update_uninstall_legacy() {
  \Drupal::service('module_installer')->uninstall(['nsw_feedback_assist']);
  return t('Please export your configuration to ensure the migrated blocks remain enabled.');
}
