<?php

declare(strict_types = 1);

namespace Drupal\nsw_feedback\Plugin\Block;

use Drupal\Component\Utility\Html;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Adds OneGov Sentiment block.
 *
 * @Block(
 *   id = "nsw_feedback_sentiment",
 *   admin_label = @Translation("NSW feedback (Sentiment check)"),
 *   category = @Translation("NSW"),
 * )
 */
class SentimentBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'environment' => 'uat',
      'widget_label' => 'Was this page helpful?',
      'description' => 'Your rating will help us improve the website.',
      'source_application' => '',
      'float_widget' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    return [
      'environment' => [
        '#type' => 'radios',
        '#options' => [
          'uat' => $this->t('UAT'),
          'production' => $this->t('Production'),
        ],
        '#description' => $this->t('Select the OneGov environment to use'),
        '#default_value' => $this->configuration['environment'],
      ],
      'widget_label' => [
        '#type' => 'textfield',
        '#title' => $this->t('Widget label'),
        '#required' => TRUE,
        '#default_value' => $this->configuration['widget_label'],
      ],
      'description' => [
        '#type' => 'textfield',
        '#title' => $this->t('Widget description'),
        '#required' => FALSE,
        '#default_value' => $this->configuration['description'],
      ],
      'source_application' => [
        '#type' => 'textfield',
        '#title' => $this->t('Source application'),
        '#required' => TRUE,
        '#default_value' => $this->configuration['source_application'],
      ],
      'float_widget' => [
        '#type' => 'checkbox',
        '#title' => $this->t('Float widget on right hand side of screen'),
        '#required' => FALSE,
        '#default_value' => $this->configuration['float_widget'],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    $this->configuration['environment'] = $form_state->getValue('environment');
    $this->configuration['widget_label'] = $form_state->getValue('widget_label');
    $this->configuration['description'] = $form_state->getValue('description');
    $this->configuration['source_application'] = $form_state->getValue('source_application');
    $this->configuration['float_widget'] = $form_state->getValue('float_widget');
  }

  /**
   * {@inheritdoc}
   */
  public function build(): array {
    $build = [
      '#theme' => 'nsw_feedback_sentiment',
      '#environment' => $this->configuration['environment'],
      '#widget_label' => $this->configuration['widget_label'],
      '#description' => $this->configuration['description'],
      '#source_application' => $this->configuration['source_application'],
      '#float_widget' => $this->configuration['float_widget'],
      '#id' => Html::getUniqueId($this->getPluginId()),
    ];

    $library = 'nsw_feedback/sentiment';
    if ($this->configuration['environment'] === 'uat') {
      $library .= '-uat';
    }
    $build['#attached']['library'][] = $library;

    return $build;
  }

}
